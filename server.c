#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#define PORT 4000

pthread_mutex_t room_m = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t client_m = PTHREAD_MUTEX_INITIALIZER;

typedef struct room_t {
  char *name;
  int max_users;
  int n_users;
  struct client_t **users;
  struct room_t *prox;
} room_t;

typedef struct client_t {
  char *name;
  int socket;
  struct sockaddr_in *addr;
  int len;
  pthread_t *thread;
  struct room_t *room; 
  struct client_t *prox;
} client_t;

int sockfd;
struct sockaddr_in serv_addr;

client_t *clients;
room_t *rooms;

int prefix(const char *pre, const char *str)
{
    int rc;
    rc = strncmp(pre, str, strlen(pre));
    return rc == 0;
}

void reply (client_t *user, char* msg) {
  char buffer[256];
  bzero(buffer, 256);
  int offset = 15;
  sprintf(buffer, "\033[1;31mServer: %s\033[1;37m", msg);
  int n = write(user->socket, buffer, 256);
  if (n < 0) 
    printf("ERROR writing to socket");
}

void create_room (char *name, int max_users) {
  room_t *room = malloc(sizeof(room_t));
  room->name = malloc(strlen(name)+1);
  strcpy (room->name,name);
  room->max_users = max_users;
  room->n_users = 0;
  room->users = malloc(sizeof(client_t *)*room->max_users);
  int i;
  for (i = 0; i < room->max_users; i++) {
    room->users[i] = NULL;
  }
  
  room->prox = NULL;
  
  pthread_mutex_lock(&room_m);
  room_t *iterator;
  for (iterator = rooms; iterator != NULL; iterator = iterator->prox) {
    if(strcmp(iterator->name, room->name) == 0) {
      puts("ERROR room already exist");
      //mutex room end
      pthread_mutex_unlock(&room_m);
      return;
    }
    if (iterator->prox == NULL) {
       iterator->prox = room;
      //mutex room end
      pthread_mutex_unlock(&room_m);
      return;
    } 
  }
  rooms = room;
  //mutex room end
  pthread_mutex_unlock(&room_m);
}

void list_room (client_t *user) {
  char buffer[256];
  int offset = 0;
  room_t *iterator;
  offset += sprintf(buffer+offset, "%s\n", "Room List");
  offset += sprintf(buffer+offset, "%s\n", "============================");
  pthread_mutex_lock(&room_m);
  for (iterator = rooms; iterator != NULL; iterator = iterator->prox) {
    offset += sprintf(buffer+offset, "%s [%d/%d]\n", iterator->name, iterator->n_users, iterator->max_users);
  }
  pthread_mutex_unlock(&room_m);
  offset += sprintf(buffer+offset, "%s", "============================");
  reply(user, buffer);
}

void enter_room (client_t *user, char *name) {
  room_t *iterator;
  //mutex room start
  pthread_mutex_lock(&room_m);
  for (iterator = rooms; iterator != NULL; iterator = iterator->prox) {
    if(strcmp(iterator->name, name) == 0) {
      if (iterator->n_users < iterator->max_users) {
        int i; 
        for (i = 0; i < iterator->max_users; i++) {
          if (iterator->users[i] == NULL) {
            iterator->users[i] = user;
            user->room = iterator;
            iterator->n_users++;
            //mutex room end 
            pthread_mutex_unlock(&room_m);
            return;
          }
        }
        reply(user, "ERROR room full");
        //mutex room end 
        pthread_mutex_unlock(&room_m);
        return;
      } else {
        reply(user, "ERROR room full");
        //mutex room end 
        pthread_mutex_unlock(&room_m);
        return;
      } 
    }      
  }
  reply(user, "ERROR room not found");
  //mutex room end  
  pthread_mutex_unlock(&room_m);
}

void leave_room (client_t *user) {
  room_t *room = user->room;
  user->room = NULL;
  int i;
  for (i = 0; i < room->max_users; i++) {
    if (room->users[i] == user) {
      room->users[i] = NULL;
      room->n_users--;
      return;
    }
  }
}

void exit_chat (client_t *user) {
  leave_room(user);
  pthread_mutex_lock(&client_m);
  if (clients == user) {
    clients = user->prox;    
  } else {
    client_t *iterator;
    for (iterator = clients; iterator->prox !=NULL; iterator->prox) {
      if(iterator->prox == user) {
        iterator->prox = iterator->prox->prox;
        break;
      }
    }
  }
  close(user->socket);
  pthread_mutex_unlock(&client_m);
  pthread_exit(0);  
}

void send_message (client_t *user, char *msg) {
  room_t *room = user->room;
  char buffer[256];
  bzero(buffer, 256);
  snprintf(buffer, 256,"\033[1;37m%s: %s", user->name, msg);
  int i;
  pthread_mutex_lock(&client_m);
  for (i = 0; i < room->max_users; i++) {
    if (room->users[i] != NULL && room->users[i] != user) {
      write(room->users[i]->socket, buffer, 256);
    }
  }
  pthread_mutex_unlock(&client_m);
}

void *client (void *arg) {
  client_t *client_handle = (client_t *) arg; 
  reply (client_handle, "Connected...");
  while (1) {
    char buffer[256];
    bzero(buffer,256);
    int n = read(client_handle->socket, buffer, 256);
    if (n < 0) {
      printf("ERROR reading from socket\n");
      exit(-1);
    }
    
    //Comandos do cliente
    if (prefix("/list", buffer)) {
      list_room(client_handle);
    } else if (prefix("/leave", buffer)) {
      leave_room (client_handle);
      enter_room (client_handle, "Geral");
    } else if (prefix("/exit", buffer)) {
      exit_chat (client_handle);
    } else if (prefix("/create ", buffer)) {
      char *name = strchr(buffer, ' ')+1;
      if(name != NULL) {
        create_room (name, 40);
      }
    } else if (prefix("/nick ", buffer)) {
      char *name = strchr(buffer, ' ')+1;
      if(name != NULL) {
        client_handle->name = malloc(strlen(name)+1);
        strcpy (client_handle->name,name);
      }
    } else if (prefix("/join ", buffer)) {
      char *name = strchr(buffer, ' ')+1;
      if(name != NULL) {
        leave_room (client_handle);
        enter_room (client_handle, name);
      }
    } else {
      send_message(client_handle, buffer);      
    }
  }

}

int main(int argc, char* argv[]) {
  puts("Server Starting...");
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    puts("ERROR opening socket");
    exit(-1);
  }

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  bzero(&(serv_addr.sin_zero), 8); 

  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
    puts("ERROR on binding");
    exit(-1);
  } 

  clients = NULL;
  rooms = NULL;
  listen(sockfd, 5);
  puts("Listening");
  create_room("Geral", 100);
  char buffer[256];
  while (1) {
    client_t *client_handle = malloc(sizeof(client_t));
    client_handle->addr = malloc(sizeof(struct sockaddr_in));
    client_handle->len = sizeof(struct sockaddr_in);
    client_handle->thread = malloc(sizeof(pthread_t));
    puts("Waiting Client");
    if ((client_handle->socket = accept(sockfd, (struct sockaddr *) client_handle->addr, &client_handle->len)) == -1) {
      puts("ERROR on accept");
      exit(-1);
    }
    puts("Client Connected");
    
    //mutex client start
    pthread_mutex_lock(&client_m);
    client_handle->prox = clients;
    clients = client_handle;
    bzero(buffer, 256);
    read(client_handle->socket, buffer, 256);
    client_handle->name = malloc(strlen(buffer)+1);
    strcpy (client_handle->name,buffer);
    pthread_mutex_unlock(&client_m);
    //mutex client end
    
    enter_room(client_handle, "Geral");
    pthread_create(client_handle->thread, NULL, client, (void *)client_handle);
  }

  return 0;
}
