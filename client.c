#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

#define PORT 4000

pthread_mutex_t client_m = PTHREAD_MUTEX_INITIALIZER;
pthread_t reader;

int sockfd;
struct sockaddr_in serv_addr;
struct hostent *host;

int prefix(const char *pre, const char *str)
{
  int rc;
  rc = strncmp(pre, str, strlen(pre));
  return rc == 0;
}

void *server (void *arg) {
  char buffer[256]; 
  while (1) {
    bzero(buffer,256);
    int n = read(sockfd, buffer, 256);
    pthread_mutex_lock(&client_m);
    if (n < 0) {
		printf("ERROR reading from socket\n");
        exit(-1); 
    }
    system("aplay -q beep.wav");
    printf("%s\n",buffer);
    pthread_mutex_unlock(&client_m);
  }
}


int main(int argc, char* argv[]) {
  if (argc < 2) {
    fprintf(stderr,"usage %s hostname\n", argv[0]);
    exit(0);
  }
  host = gethostbyname(argv[1]);
  if (host == NULL) {
    fprintf(stderr,"ERROR, no such host\n");
    exit(0);
  }
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
    printf("ERROR opening socket\n");

  serv_addr.sin_family = AF_INET;     
  serv_addr.sin_port = htons(PORT);    
  serv_addr.sin_addr = *((struct in_addr *)host->h_addr);
  bzero(&(serv_addr.sin_zero), 8);     
  if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
    printf("ERROR connecting\n");
    exit(-1);
  }
  char buffer[256];
  bzero(buffer,256);
  printf("Digite o seu nickname: ");
  fgets(buffer, 20, stdin);
  char *pos;
  if ((pos=strchr(buffer, '\n')) != NULL)
    *pos = '\0';
  write(sockfd, buffer, 256);
  pthread_create(&reader, NULL, server, NULL);
  while (1) {
    bzero(buffer,256);
    __fpurge(stdin);
    fgets(buffer, 256, stdin);
    pthread_mutex_lock(&client_m);
    if ((pos=strchr(buffer, '\n')) != NULL)
      *pos = '\0';
    write(sockfd, buffer, 256);
    if (prefix("/exit", buffer)) {
      close(sockfd);
      puts("Desligando...");
      pthread_mutex_unlock(&client_m);
      exit(0);
    }
    pthread_mutex_unlock(&client_m);

  }
  return 0;
}
